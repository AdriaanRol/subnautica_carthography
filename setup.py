#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("CHANGELOG.rst") as history_file:
    history = history_file.read()

requirements = [
    "pandas",
    "numpy",
    "lmfit",
    "scipy",
    "cmocean",
    "matplotlib",
    "typing_extensions",
]

setup_requirements = [
    "pytest-runner",
    "wheel",
]

test_requirements = [
    "pytest>=3",
]

setup(
    author="Adriaan Rol",
    author_email="adriaan.rol@gmail.com",
    python_requires=">=3.5",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    description="Some tools to aid in mapping out subnautica by hand.",
    install_requires=requirements,
    license="BSD license",
    long_description=readme + "\n\n" + history,
    include_package_data=True,
    keywords="subnautica_carthography",
    name="subnautica_carthography",
    packages=find_packages(
        include=["subnautica_carthography", "subnautica_carthography.*"]
    ),
    setup_requires=setup_requirements,
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.com/AdriaanRol/subnautica_carthography",
    version="0.1.0",
    zip_safe=False,
)
