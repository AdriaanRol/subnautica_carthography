"""
Used to log coordinates and write observations to files
"""
import fnmatch
import os
import numbers
import subnautica_carthography as sc
import subnautica_carthography.positioning as sp
import subnautica_carthography.visualization as vis
import pandas as pd
import matplotlib.pyplot as plt
from typing import Tuple, List
import datetime

# harcoded datadir within the project folder. Can be changed when using it.
LOGGING_DIR = os.path.abspath(os.path.join(sc.__path__[0], "..", "logged_coordinates"))


class CoordinateLogger:
    def __init__(self, depth_log_fn="depths", beacon_log_fn="beacons", verbose=True):
        self.DEPTH_LOG_FN = depth_log_fn
        self.BEACON_LOG_FN = beacon_log_fn

        self.verbose = verbose

    @property
    def beacon_log(self):
        for filename in sorted(os.listdir(LOGGING_DIR), reverse=True):

            # files are prefixed with a datestamp.
            # sorting by the first file
            if fnmatch.fnmatch(filename, f"*_{self.BEACON_LOG_FN}.csv"):

                fp = os.path.join(LOGGING_DIR, filename)
                beacon_log = pd.read_csv(fp, index_col=0)
                return beacon_log

        print("No beacon logfile was found. Creating empty logfile.")
        # create a new empty dataframe with the right columns
        beacon_log = pd.DataFrame(columns=["beacon", "x", "y", "z", "added"])
        # save it to the logging directory
        self._write_beacon_log(beacon_log)

        return beacon_log

    def _write_beacon_log(self, beacon_log):
        today = datetime.date.today()
        datestring = today.strftime("%y%m%d")
        fn = f"{datestring}_{self.BEACON_LOG_FN}.csv"
        fp = os.path.join(LOGGING_DIR, fn)
        if self.verbose:
            print(f"Writing beacon log to {fp}")
        beacon_log.to_csv(fp)

    @property
    def depth_log(self):
        for filename in sorted(os.listdir(LOGGING_DIR), reverse=True):

            # files are prefixed with a datestamp.
            # sorting by the first file
            if fnmatch.fnmatch(filename, f"*_{self.DEPTH_LOG_FN}.csv"):
                fp = os.path.join(LOGGING_DIR, filename)
                depth_log = pd.read_csv(fp, index_col=0)
                return depth_log

        print("No depth logfile was found. Creating empty logfile.")
        # create a new empty dataframe with the right columns
        depth_log = pd.DataFrame(columns=["x", "y", "z", "added"])
        # save it to the logging directory
        self._write_depth_log(depth_log)

        return depth_log

    def _write_depth_log(self, depth_log):
        today = datetime.date.today()
        datestring = today.strftime("%y%m%d")
        fn = f"{datestring}_{self.DEPTH_LOG_FN}.csv"
        fp = os.path.join(LOGGING_DIR, fn)
        if self.verbose:
            print(f"Writing depth log to {fp}")
        depth_log.to_csv(fp)

    def log_beacon(
        self,
        name: str,
        observations: List[Tuple[str, float]],
        depth: float,
        x_guess: float = 0,
        y_guess: float = 0,
    ):
        """
        Determines location of a beacon and adds it to the beacon log.

        name:
            the name of the beacon to be added
        observations
            list of tuples with (ref_beacon: str, distance: float)
        depth:
            the current depth (can be >0, <0 or None)
        """

        # input is chosen to be convenient to type.
        beacon_names, distances = zip(*observations)

        coords_fit = sp.fit_observations(
            distances=distances,
            beacon_names=beacon_names,
            depth=depth,
            x_guess=x_guess,
            y_guess=y_guess,
            beacon_log=self.beacon_log,
        )
        x = coords_fit.params['x']
        y = coords_fit.params['y']
        z = coords_fit.params['z']

        if x.stderr>2 or y.stderr>2 or z.stderr>2:
            print("Fit did not converge. Not logging data, returning fit result.")
            return coords_fit


        self.write_coords_to_beacon_log(name, x.value, y.value, z.value)

        return self.beacon_log

    def log_depth(
        self,
        observations: List[Tuple[str, float]],
        depth: float,
        x_guess: float = 0,
        y_guess: float = 0,
    ):
        """
        Determines your current location and adds it to the depth log.

        observations
            list of tuples with (ref_beacon: str, distance: float)
        depth:
            the current depth (can be >0, <0 or None)
        """

        # input is chosen to be convenient to type.
        beacon_names, distances = zip(*observations)

        coords_fit = sp.fit_observations(
            distances=distances,
            beacon_names=beacon_names,
            depth=depth,
            beacon_log=self.beacon_log,
            x_guess=x_guess,
            y_guess=y_guess,
        )
        x = coords_fit.params['x']
        y = coords_fit.params['y']
        z = coords_fit.params['z']

        if x.stderr>2 or y.stderr>2 or z.stderr>2:
            print("Fit did not converge. Not logging data, returning fit result.")
            return coords_fit


        self.write_coords_to_depth_log(x.value, y.value, z.value)

        return self.depth_log

    def write_coords_to_beacon_log(self, name, x, y, z):
        """
        Writes the coordinates of a beacon to the log.

        Only use this if the coordinates are known.
        Normally you want to use :meth:`log_beacon`
        """
        # type-checking inputs before writing to log
        assert isinstance(name, str)
        assert isinstance(x, numbers.Number)
        assert isinstance(y, numbers.Number)
        assert isinstance(z, numbers.Number)

        beacon_log = self.beacon_log

        beacon_log.loc[len(beacon_log)] = [
            name,
            x,
            y,
            z,
            datetime.datetime.now(),
        ]

        if self.verbose:
            print(f"writing {beacon_log.loc[len(beacon_log)-1]} to beacon log")

        self._write_beacon_log(beacon_log)

    def write_coords_to_depth_log(self, x, y, z):
        """
        Writes the coordinates of a beacon to the log.

        Only use this if the coordinates are known.
        Normally you want to use :meth:`log_depth`
        """
        # type-checking inputs before writing to log
        assert isinstance(x, numbers.Number)
        assert isinstance(y, numbers.Number)
        assert isinstance(z, numbers.Number)

        depth_log = self.depth_log

        depth_log.loc[len(depth_log)] = [
            x,
            y,
            z,
            datetime.datetime.now(),
        ]

        if self.verbose:
            print(f"writing {depth_log.loc[len(depth_log)-1]} to depth log")

        self._write_depth_log(depth_log)

    def make_nautical_chart(self, ax: plt.Axes = None, mark_beacons: bool = True):
        """
        Makes a nautical chart based on a linear interpolation of the logged depths.

        if mark_beacons is True, all beacons will be marked on the map.

        Note that this requires more than 4 depths to be logged as otherwise the
        interpolation is not sensible.
        """
        f, ax = vis.plot_nautical_chart(coords=self.depth_log)

        if mark_beacons:
            # marks beacon in Orange (color C1)
            ax.scatter(self.beacon_log["x"], self.beacon_log["y"], c="C1")
            for _, beacon in self.beacon_log.iterrows():
                ax.annotate(beacon["beacon"], (beacon["x"], beacon["y"]))

        today = datetime.date.today()
        datestring = today.strftime("%y%m%d")

        fn = f"{datestring}_nautical_map.svg"
        fp = os.path.join(LOGGING_DIR, fn)
        f.savefig(fp)

        return f, ax
