=======================
subnautica_carthography
=======================


.. image:: https://gitlab.com/AdriaanRol/subnautica_carthography/badges/main/pipeline.svg
    :target: https://gitlab.com/AdriaanRol/subnautica_carthography/pipelines/
    :alt: Build Status

.. image:: https://img.shields.io/pypi/v/subnautica_carthography.svg
    :target: https://pypi.org/pypi/subnautica_carthography
    :alt: PyPI

.. image:: https://gitlab.com/AdriaanRol/subnautica_carthography/badges/main/coverage.svg
    :target: https://gitlab.com/AdriaanRol/subnautica_carthography/pipelines/
    :alt: Coverage


.. image:: https://readthedocs.org/projects/subnautica-carthography/badge/?version=latest
        :target: https://subnautica-carthography.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


.. image:: https://img.shields.io/badge/License-BSD license-blue.svg
    :target: https://gitlab.com/AdriaanRol/subnautica_carthography/-/blob/main/LICENSE





Some tools to aid in mapping out subnautica by hand


* Free software: BSD license
* Documentation: https://subnautica-carthography.readthedocs.io.




Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `cookiecutter-pypackage-gitlab`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-pypackage-gitlab`: https://gitlab.com/AdriaanRol/cookiecutter-pypackage-gitlab
