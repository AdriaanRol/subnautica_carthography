from subnautica_carthography.positioning import default_beacons
import subnautica_carthography.visualization as vis

def test_plot_nautical_chart():


    f, ax = vis.plot_nautical_chart(default_beacons, mark_depths=True)

    assert ax.get_xlim() == (-1000, 1000)
    assert ax.get_ylim() == (-1000, 1000)