import pytest
import tempfile
import pandas as pd
from subnautica_carthography import coordinate_logging as cl
from subnautica_carthography import positioning as sp


@pytest.fixture(scope="module", autouse=False)
def coordinate_logger():

    with tempfile.TemporaryDirectory(prefix="pytest-<project-name>-") as temp_dir:
        cl.LOGGING_DIR = temp_dir

        clog = cl.CoordinateLogger(
            beacon_log_fn="beacon_testfile", depth_log_fn="depths_testfile"
        )
        yield clog


def test_beacon_log_starts_empty(coordinate_logger):
    assert isinstance(coordinate_logger.beacon_log, pd.DataFrame)
    assert len(coordinate_logger.beacon_log) == 0


def test_depth_log_starts_empty(coordinate_logger):
    assert isinstance(coordinate_logger.depth_log, pd.DataFrame)
    assert len(coordinate_logger.depth_log) == 0


@pytest.fixture(scope="module", autouse=False)
def coordinate_logger_with_ref_beacons():

    with tempfile.TemporaryDirectory(prefix="pytest-<project-name>-") as temp_dir:
        cl.LOGGING_DIR = temp_dir

        clog = cl.CoordinateLogger(beacon_log_fn="beacons", depth_log_fn="depths")

        clog.write_coords_to_beacon_log("LP5", 0, 0, 0)
        clog.write_coords_to_beacon_log("N1", 0, 100, 0)
        clog.write_coords_to_beacon_log("E1", 100, 0, 0)
        clog.write_coords_to_beacon_log("S1", 0, -100, 0)
        clog.write_coords_to_beacon_log("W1", 100, 0, 0)

        yield clog




def test_ref_beacons_in_log(coordinate_logger_with_ref_beacons):
    clog = coordinate_logger_with_ref_beacons

    assert isinstance(clog.beacon_log, pd.DataFrame)
    assert list(clog.beacon_log["beacon"]) == ["LP5", "N1", "E1", "S1", "W1"]


def test_log_beacon(coordinate_logger_with_ref_beacons):
    clog = coordinate_logger_with_ref_beacons

    TEST_LOCATION = (312, 273, -129)
    d_LP5 = sp.distance_to_beacon(*TEST_LOCATION, 0, reference_beacons=clog.beacon_log)
    d_N1 = sp.distance_to_beacon(*TEST_LOCATION, 1, reference_beacons=clog.beacon_log)
    d_E1 = sp.distance_to_beacon(*TEST_LOCATION, 2, reference_beacons=clog.beacon_log)
    d_S1 = sp.distance_to_beacon(*TEST_LOCATION, 3, reference_beacons=clog.beacon_log)
    observations = [("LP5", d_LP5), ("N1", d_N1), ("E1", d_E1), ("S1", d_S1)]
    clog.log_beacon("T1", observations, depth=129)

    added_beacon = clog.beacon_log.iloc[-1]
    assert added_beacon["beacon"] == "T1"
    assert added_beacon["x"] == pytest.approx(TEST_LOCATION[0], 0.1)
    assert added_beacon["y"] == pytest.approx(TEST_LOCATION[1], 0.1)
    assert added_beacon["z"] == pytest.approx(TEST_LOCATION[2], 0.1)


def test_log_depth(coordinate_logger_with_ref_beacons):
    clog = coordinate_logger_with_ref_beacons

    # check that the depth_log starts out empty
    assert isinstance(clog.depth_log, pd.DataFrame)
    assert len(clog.depth_log) == 0

    TEST_LOCATION = (312, 273, -129)
    d_LP5 = sp.distance_to_beacon(*TEST_LOCATION, 0, reference_beacons=clog.beacon_log)
    d_N1 = sp.distance_to_beacon(*TEST_LOCATION, 1, reference_beacons=clog.beacon_log)
    d_E1 = sp.distance_to_beacon(*TEST_LOCATION, 2, reference_beacons=clog.beacon_log)
    d_S1 = sp.distance_to_beacon(*TEST_LOCATION, 3, reference_beacons=clog.beacon_log)
    observations = [("LP5", d_LP5), ("N1", d_N1), ("E1", d_E1), ("S1", d_S1)]

    clog.log_depth(observations, depth=129)

    added_depth = clog.depth_log.iloc[-1]
    assert added_depth["x"] == pytest.approx(TEST_LOCATION[0], 0.1)
    assert added_depth["y"] == pytest.approx(TEST_LOCATION[1], 0.1)
    assert added_depth["z"] == pytest.approx(TEST_LOCATION[2], 0.1)


def test_make_nautical_chart(coordinate_logger_with_ref_beacons):
    clog = coordinate_logger_with_ref_beacons

    clog.write_coords_to_depth_log(0, 0, -20)
    clog.write_coords_to_depth_log(0, 100, -50)
    clog.write_coords_to_depth_log(100, 0, -30)
    clog.write_coords_to_depth_log(0, -100, -150)
    clog.write_coords_to_depth_log(-100, 0, 15)

    f, ax = clog.make_nautical_chart()


@pytest.fixture(scope="module", autouse=False)
def coordinate_logger_with_ref_beacons5():

    with tempfile.TemporaryDirectory(prefix="pytest-<project-name>-") as temp_dir:
        cl.LOGGING_DIR = temp_dir

        clog = cl.CoordinateLogger(beacon_log_fn="beacons", depth_log_fn="depths")

        clog.write_coords_to_beacon_log("LP5", 0, 0, 0)
        clog.write_coords_to_beacon_log("S5", 0, -500, 0)
        clog.write_coords_to_beacon_log("N5", 0, 500, 0)
        clog.write_coords_to_beacon_log("W5", -500, 0, 0)
        clog.write_coords_to_beacon_log("E5", 500, 0, 0)

        yield clog


def test_log_depth_5(coordinate_logger_with_ref_beacons5):
    clog = coordinate_logger_with_ref_beacons5

    TEST_LOCATION = (312, 20, -47)
    d_LP5 = sp.distance_to_beacon(*TEST_LOCATION, 0, reference_beacons=clog.beacon_log)
    d_S5 = sp.distance_to_beacon(*TEST_LOCATION, 1, reference_beacons=clog.beacon_log)
    d_N5 = sp.distance_to_beacon(*TEST_LOCATION, 2, reference_beacons=clog.beacon_log)
    # d_W5 = sp.distance_to_beacon(*TEST_LOCATION, 2)
    d_W5 = sp.distance_to_beacon(*TEST_LOCATION, 3, reference_beacons=clog.beacon_log)
    d_E5 = sp.distance_to_beacon(*TEST_LOCATION, 4, reference_beacons=clog.beacon_log)


    observations = [("LP5", d_LP5), ("N5", d_N5), ("E5", d_E5), ("S5", d_S5)]

    clog.log_depth(observations, depth=47)


    added_depth = clog.depth_log.iloc[-1]
    assert added_depth["x"] == pytest.approx(TEST_LOCATION[0], 0.1)
    assert added_depth["y"] == pytest.approx(TEST_LOCATION[1], 0.1)
    assert added_depth["z"] == pytest.approx(TEST_LOCATION[2], 0.1)
