import pytest
import pandas as pd
from pytest import approx
import numpy as np
import subnautica_carthography.positioning as sp


TEST_LOCATION = (312, 273, -129)


def test_distance_to_beacon():

    # test horizontal x distance


    assert sp.distance_to_beacon(100, 0, 0, beacon_ID=0) == 100
    assert sp.distance_to_beacon(0, 100, 0, beacon_ID=0) == 100
    assert sp.distance_to_beacon(0, 0, 100, beacon_ID=0) == 100
    assert sp.distance_to_beacon(-100, 0, 0, beacon_ID=0) == 100
    assert sp.distance_to_beacon(0, -100, 0, beacon_ID=0) == 100
    assert sp.distance_to_beacon(0, 0, -100, beacon_ID=0) == 100


    # test horizontal y distance
    assert sp.distance_to_beacon(0, 0, 0, beacon_ID=1) == 1000

    # x,y combined distance
    assert sp.distance_to_beacon(220, 0, 0, beacon_ID=1) == np.sqrt(
        1000 ** 2 + 220 ** 2
    )

    # y,z combined distance
    assert sp.distance_to_beacon(0, 0, -220, beacon_ID=1) == np.sqrt(
        1000 ** 2 + 220 ** 2
    )

def test_distance_to_beacon_custom_beacons():

    _custom_beacons = [
        ("LP5", 0, 0, 0),
        ("N5", 0, 500, 0),
        ("E5", 500, 0, 0),
        ("S5", 0, -500, 0),
        ("W5", -500, 0, 0),
    ]

    ref_beacons = pd.DataFrame(_custom_beacons, columns=["beacon", "x", "y", "z"])


    sp._distance_to_beacons(0, 0, 0, beacon_IDs=[1, 4], reference_beacons=ref_beacons) == [500, 500]




@pytest.fixture(scope="module", autouse=False)
def generate_test_obervations():

    # generate some observations
    distances = []
    beacon_names = []

    for i, beacon_name in enumerate(sp.default_beacons.beacon):
        beacon_names.append(beacon_name)
        d = sp.distance_to_beacon(*TEST_LOCATION, beacon_ID=i)
        distances.append(d)

    return beacon_names, distances


def test_fit_observations_free(generate_test_obervations):
    beacon_names, distances = generate_test_obervations

    coords_fit = sp.fit_observations(distances=distances, beacon_names=beacon_names)

    coords = coords_fit.best_values

    assert coords["x"] == approx(TEST_LOCATION[0])
    assert coords["y"] == approx(TEST_LOCATION[1])
    assert coords["z"] == approx(TEST_LOCATION[2])


def test_fit_observations_below_sea(generate_test_obervations):
    beacon_names, distances = generate_test_obervations

    coords_fit = sp.fit_observations(
        distances=distances, beacon_names=beacon_names, depth="<0"
    )

    coords = coords_fit.best_values
    assert coords["x"] == approx(TEST_LOCATION[0])
    assert coords["y"] == approx(TEST_LOCATION[1])
    assert coords["z"] == approx(TEST_LOCATION[2])


def test_fit_observations_above_sea(generate_test_obervations):
    beacon_names, distances = generate_test_obervations

    coords_fit = sp.fit_observations(
        distances=distances, beacon_names=beacon_names, depth=">0"
    )
    coords = coords_fit.best_values

    assert coords["x"] == approx(TEST_LOCATION[0])
    assert coords["y"] == approx(TEST_LOCATION[1])
    # find the "other" solution, above the plane.
    assert coords["z"] == approx(-TEST_LOCATION[2])


def test_determine_beacon_IDs():
    beacon_names = ["LP5", "S1", "N1"]
    beacon_IDs = sp.determine_beacon_IDs(beacon_names, beacons=sp.default_beacons)
    assert beacon_IDs == [0, 3, 1]
