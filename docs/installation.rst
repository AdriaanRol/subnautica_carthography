.. highlight:: shell

============
Installation
============


Stable release
--------------

To install subnautica_carthography, run this command in your terminal:

.. code-block:: console

    $ pip install subnautica_carthography

This is the preferred method to install subnautica_carthography, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for subnautica_carthography can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.com/AdriaanRol/subnautica_carthography

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://gitlab.com/AdriaanRol/subnautica_carthography/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Gitlab repo: https://gitlab.com/AdriaanRol/subnautica_carthography
.. _tarball: https://gitlab.com/AdriaanRol/subnautica_carthography/tarball/master
